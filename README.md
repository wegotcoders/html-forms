# Acme Tours Booking Form #

Using HTML to create a booking form for a tour company.

### Clone Repository ###

1. Fork this repository.
2. Git clone your own copy to your assignments folder.
3. Change directory into your local copy of the repo.

### To Do ###

1. Open booking_form.html in Sublime, notice the script tags in the head, they are there to bring in some javascript.
2. Think about what information is needed to book a tour, for example; names, emails, tour type, dates, cost, etc.
3. Use your knowledge of creating forms to design a booking form for a tour company.
4. Use the submit.js file to handle the form submission, modify the message if you like.
5. Try to use as many different types of form controls as you can.  Experiment with different input types.
6. You can see your progress by typing in terminal 'open booking_form.html'.